package mx.edu.uacm.modelo;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.AfterEach;

import org.junit.jupiter.api.Assertions;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class PacienteTest {
	private Paciente paciente;

	@BeforeEach
	void inicio() throws Exception {
		System.out.println("Inicio test");
		paciente = new Paciente();
	}

	@AfterEach
	void tearDown() throws Exception {
		System.out.println("Fin test");
	}

	/*
	 * 
	 * 
	 * Pruebas de métodos set
	 * 
	 * 
	 */

	@DisplayName("Porbando valores válidos para nombre")
	@Test
	void testSetNombre() {
		try {
			paciente.setNombre("Edwar");
			assertEquals("EDWAR", paciente.getNombre());
		} catch (ExcepcionParametros e) {
			System.out.print(e.getMessage());
		}
	}

	@DisplayName("Porbando valores  NO válidos para nombre a traves de excepciones")
	@Test
	void testSetNombreInvalido() {
		assertThrows(ExcepcionParametros.class, () -> paciente.setNombre("Edwar123"));
	}

	@DisplayName("Probar apellido válido para el metodo setApellido()")
	@Test
	public void testSetApellido() {
		try {
			paciente.setApellido("Garcia");
			assertEquals("GARCIA", paciente.getApellido());
		} catch (ExcepcionParametros e) {
			System.out.print(e.getMessage());
		}
	}

	@DisplayName("Probar apellido NO válido para el metodo setApellido() a traves de excepciones")
	@Test
	public void testSetApellidoInvalido() {
		try {
			paciente.setApellido("Garcia123");
		} catch (ExcepcionParametros e) {
			assertEquals("Apellido no válido\n.", e.getMessage());
		}
	}

	@DisplayName("Probar peso valido para el metodo setPeso()")
	@Test
	public void testSetPeso() {
		assertDoesNotThrow(() -> paciente.setPeso(80.0));
		assertTrue(paciente.getPeso() == 80.0);
	}

	// PARA PROBAR VALORES INVÁLIDOS SIEMPRE SE DEBEN DE USAR EXPRESIONES
	@DisplayName("Probar peso NO válido para el método setPeso()")
	@Test
	public void testSetPesoInvalido() {
		assertThrows(ExcepcionParametros.class, () -> paciente.setPeso(-100.0));
	}

	@DisplayName("Probar estatura válido para el método setEstatura()")
	@Test
	public void testSetEstatura() {
		assertDoesNotThrow(() -> paciente.setEstatura(1.84));
		assertTrue(paciente.getEstatura() == 1.84);
	}

	@DisplayName("Probar estatura NO válido para el método setEstatura()")
	@Test
	public void testSetEstaturaInvalida() {
		assertThrows(ExcepcionParametros.class, () -> paciente.setEstatura(-100.0));

	}

	@DisplayName("Prueba para clasificación no válida, sin establecer peso y estatura")
	@Test
	void testExcepcionClasificacion() {
		assertThrows(ExcepcionParametros.class, () -> paciente.determinarClasificacion());
	}

	/*
	 * 
	 * 
	 * Pruebas de particion de equivalencias
	 * 
	 * 
	 */

	@DisplayName("Pruebas de PE para valores válidos para determinarClasificacion() CP1 - CP4")
	@ParameterizedTest(name = "IMC = {0}, {1}")
	@CsvSource({ 
		"12.4, Clasificación: Peso bajo",
		"20, Clasificación: Normal",
		"27.6, Clasificación: Sobrepeso",
		"35, Clasificación: Obesidad" 
	})
	void testClasificacionParticionValidos(double imc, String clasificacion) {
		try {
			paciente.setEstatura(1.2);
			paciente.setPeso(imc * Math.pow(paciente.getEstatura(), 2));
			assertEquals(clasificacion, paciente.determinarClasificacion());
		} catch (ExcepcionParametros e) {
			System.out.println(e.getMessage());
		}
	}

	@DisplayName("Pueba de valores validos calculo IMC CP5 P_E")
	@Test
	void indiceIMC() {
		try {
			paciente.setPeso(80.0);
			paciente.setEstatura(1.84);
			assertEquals(23.62, paciente.calcularImc(), 0.1);
		} catch (ExcepcionParametros e) {
			System.out.println(e.getMessage());
		}
	}

	@ParameterizedTest(name = "peso {0} estatura={1} imc={2}")
	@CsvSource({
		"-50.0,1.5,-22.22",
		"140,-42.3,0.078",
		"5000,0.5,20000",
		"200,50,0.08" 
	})
	@DisplayName("Prueba de IMC con Parametros CP1-CP4 P_E ")
	void indiceIMCTestP(double peso, double estatura, double resultado) {
		try {
			paciente.setPeso(peso);
			paciente.setEstatura(estatura);
			Assertions.assertEquals(resultado, paciente.calcularImc(), 0.1);
		} catch (ExcepcionParametros e) {

		}
	}

	@DisplayName("Pueba de expcion de clasificacion CP5 P_E")
	@Test
	void clasificacionIMCCP5() {
		try {
			paciente.setPeso(-10.0);
			paciente.setEstatura(1.0);
			assertThrows(ExcepcionParametros.class, () -> paciente.determinarClasificacion());
		} catch (ExcepcionParametros e) {
			System.out.println(e.getMessage());
		}
	}

	@DisplayName("Pruebas de VL válidos para determinarClasificacion(). CP4 - CP8")
	@ParameterizedTest(name = "IMC = {0}, {1}")
	@CsvSource({ 
		"0.27, Clasificación: Peso bajo",
		"18.5, Clasificación: Normal",
		"25, Clasificación: Sobrepeso",
		"30, Clasificación: Obesidad",
		"1875, Clasificación: Obesidad"
	})
	void testClasificacionLimiteValidos(double imc, String clasificacion) {
		try {
			paciente.setEstatura(1.3);
			paciente.setPeso(imc * Math.pow(paciente.getEstatura(), 2));
			assertEquals(clasificacion, paciente.determinarClasificacion());
		} catch (ExcepcionParametros e) {
			System.out.println(e.getMessage());
		}
	}

	@ParameterizedTest(name = "peso {0} estatura={1}")
	@CsvSource({ 
		"0.269999,1.0",
		"1875.000001,1.0"
	})
	@DisplayName("Prueba de IMC con Parametros CP1-CP2 V_L ")
	void clasificacionIMC_VL(double peso, double estatura) {
		try {
			paciente.setPeso(peso);
			paciente.setEstatura(estatura);
			assertThrows(ExcepcionParametros.class, () -> paciente.determinarClasificacion());
		} catch (ExcepcionParametros e) {

		}
	}
}
