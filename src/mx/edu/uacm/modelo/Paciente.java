package mx.edu.uacm.modelo;

import java.util.regex.Pattern;

public class Paciente {
	//Atributos
	private String id_expediente;
	private String nombre;
	private String apellido;
    private double peso;
    private double estatura;
	private static final String REGEX = "^([A-ZÁÉÍÓÚÑ]{1}[A-ZÁÉÍÓÚÑ]*)+$";
    private static final double PESO_MAX = 300;
	private static final double ESTATURA_MAX = 3;
    private static final double PESO_MIN = 2.5;
	private static final double ESTATURA_MIN = 0.4;

//Metodos get y set
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) throws ExcepcionParametros {
     nombre=nombre.toUpperCase();
	 boolean esNombre = Pattern.matches(REGEX, nombre);
		if(esNombre==true)
			this.nombre=nombre;
		else
			throw new ExcepcionParametros("Nombre no válido.\n");
			
	}
	
	public String getApellido() {
		return apellido;
	}
	
	public void setApellido(String apellido) throws ExcepcionParametros {
	    apellido=apellido.toUpperCase();
        boolean esApellido = Pattern.matches(REGEX, apellido);
		if(esApellido==true)
			this.apellido=apellido;
		else
			throw new ExcepcionParametros("Apellido no válido\n.");
	}
	
	public double getPeso() {
		return peso;
	}
	
	public void setPeso(Double peso) throws ExcepcionParametros {
		if(peso>PESO_MIN && peso < PESO_MAX)
			this.peso=peso;
		else
			throw new ExcepcionParametros("No puedes ingresar un peso que sea NEGATIVO o igual a 0.");
	}
	
	public double getEstatura() {
		return estatura;
	}
	
	public void setEstatura(Double estatura) throws ExcepcionParametros {
		if(estatura>ESTATURA_MIN && estatura < ESTATURA_MAX)
		    this.estatura=estatura;
		else
			throw new ExcepcionParametros("No puedes ingresar una estatura que sea NEGATIVO o igual a 0.");
	}
	
	public  double calcularImc() {
		return ((peso)/(estatura*estatura));
	}
	
	//METODOS PARA CALCULAR EL IMC
public String determinarClasificacion() throws ExcepcionParametros{
	double imc=calcularImc();
	String resultado="Clasificación: ";

	if(estatura>ESTATURA_MIN && peso>PESO_MIN)
	{
	 if(imc<18.5)
		 resultado+="Peso bajo";
	 if(imc>=18.5 && imc<25)
		 resultado+="Normal";
	 if(imc>=25 && imc <30)
		 resultado+="Sobrepeso";
	 if(imc>=30)
		 resultado+="Obesidad";
	}else
		throw new ExcepcionParametros("Estatura o peso no válido.");
	return resultado;
}
//FUNCION PRINCIPAL
	public static void main(String[] args) throws ExcepcionParametros {
	/*Paciente p1 = new Paciente("Edwar12","Garcia",80.0,1.84);
	p1.estadoNutricional(p1.peso, p1.estatura);*/
	Paciente p2 = new Paciente();	
	p2.setNombre("MARIANO");
	p2.setApellido("ROMERO");
	p2.setPeso(81.0);
	p2.setEstatura(1.82);
	
	//p2.estadoNutricional2(p2);
	
	Paciente p3 = new Paciente();	
	p2.setNombre("Luis");
	p2.setApellido("Calderon");
	p2.setPeso(65.0);
	p2.setEstatura(1.75);
; }
}

