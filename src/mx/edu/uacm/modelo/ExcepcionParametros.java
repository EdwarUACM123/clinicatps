package mx.edu.uacm.modelo;

public class ExcepcionParametros extends Exception {
	
	private static final long serialVersionUID = 1L;

	public ExcepcionParametros (String mensaje) {
		super(mensaje);
	}
}

